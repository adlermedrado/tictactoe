[![build status](https://gitlab.com/adlermedrado/tictactoe/badges/master/build.svg)](https://gitlab.com/adlermedrado/tictactoe/commits/master)

[![coverage report](https://gitlab.com/adlermedrado/tictactoe/badges/master/coverage.svg)](https://gitlab.com/adlermedrado/tictactoe/commits/master)

# TicTacToe project - Backend

Follow the steps below to run this project:

* Clone this repo
* [Install composer](https://getcomposer.org/download/)
* This project runs only with PHP 7.1+
* Run composer.phar install 
* Rename `config/development.config.php.dist` to `config\development.config.php`
* Rename `config/autoload/development.local.php.dist` to `config/autoload/development.local.php`
* Rename `config/autoload/local.php.dist` to `config/autoload/local.php`
* Start PHP Built-in server: `php -S 0.0.0.0:8080 -t public`
* The API will can be accessed through `http://0.0.0.0:8080/v1/api/ping` to test
* To connect to the backend API, use the client that you can download from this address: https://gitlab.com/adlermedrado/tictactoe-client