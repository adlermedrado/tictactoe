<?php

namespace AppTest\Domain\Game\Board;

use App\Domain\Game\Board\Cell;
use App\Domain\Game\Board\Exception\CellAlreadyUsedException;
use App\Domain\Game\Board\Exception\IllegalCellPositionException;
use App\Domain\Game\Player\Bot;
use App\Domain\Game\Player\User;
use PHPUnit\Framework\TestCase;

class CellTest extends TestCase
{
    public function testCellSuccess()
    {
        $cell = new Cell(0);
        $this->assertContains($cell->getPosition(), Cell::AVAILABLE_POSITIONS);
        $cell2 = new Cell(1);
        $this->assertContains($cell2->getPosition(), Cell::AVAILABLE_POSITIONS);
        $cell3 = new Cell(2);
        $this->assertContains($cell3->getPosition(), Cell::AVAILABLE_POSITIONS);
    }

    public function testCellInvalidPosition()
    {
        $this->expectException(IllegalCellPositionException::class);
        new Cell(4);
    }

    public function testCellAlreadyUsed()
    {
        $this->expectException(CellAlreadyUsedException::class);
        $cell = new Cell(0);
        $cell->setPosition(0);
    }

    public function testAddPlayerUser()
    {
        $cell = new Cell(1);
        $cell->setPlayer(new User(User::PLAYER_TYPE_USER));
        $this->assertEquals(User::PLAYER_TYPE_USER, $cell->getPlayer());
    }

    public function testAddPlayerBot()
    {
        $cell = new Cell(1);
        $cell->setPlayer(new Bot(Bot::PLAYER_TYPE_BOT));
        $this->assertEquals(Bot::PLAYER_TYPE_BOT, $cell->getPlayer());
    }
}