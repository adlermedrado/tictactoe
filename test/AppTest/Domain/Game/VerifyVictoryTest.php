<?php

namespace AppTest\Domain\Game;

use App\Domain\Game\Board\Board;
use App\Domain\Game\VerifyVictory;
use PHPUnit\Framework\TestCase;

class VerifyVictoryTest extends TestCase
{
    public function testcheckVictory()
    {
        $verifyVictory = new VerifyVictory();
        $board = new Board();

        $boardState = [
            ['X', '', ''],
            ['X', '', ''],
            ['X', '', '']
        ];

        $board->updateState($boardState);
        $verifyVictory->setBoard($board);
        $this->assertTrue($verifyVictory->verify());

        $board = new Board();
        $boardState = [
            ['', 'X', ''],
            ['', 'X', ''],
            ['', 'X', '']
        ];
        $board->updateState($boardState);
        $verifyVictory->setBoard($board);
        $this->assertTrue($verifyVictory->verify());

        $boardState = [
            ['', '', 'X'],
            ['', '', 'X'],
            ['', '', 'X']
        ];
        $board->updateState($boardState);
        $verifyVictory->setBoard($board);
        $this->assertTrue($verifyVictory->verify());

        $board = new Board();
        $boardState = [
            ['', '', ''],
            ['', '', ''],
            ['X', 'X', 'X']
        ];
        $board->updateState($boardState);
        $verifyVictory->setBoard($board);
        $this->assertTrue($verifyVictory->verify());

        $boardState = [
            ['X', '', ''],
            ['', 'X', ''],
            ['', '', 'X']
        ];
        $board->updateState($boardState);
        $verifyVictory->setBoard($board);
        $this->assertTrue($verifyVictory->verify());

        $board = new Board();
        $boardState = [
            ['', '', 'O'],
            ['', 'O', ''],
            ['O', '', '']
        ];
        $board->updateState($boardState);
        $verifyVictory->setBoard($board);
        $this->assertTrue($verifyVictory->verify());
    }

    public function testCheckVitoryWithoutWinner()
    {
        $verifyVictory = new VerifyVictory();
        $board = new Board();

        $boardState = [
            ['', '', ''],
            ['', 'O', ''],
            ['', '', 'X']
        ];

        $board->updateState($boardState);
        $verifyVictory->setBoard($board);
        $this->assertFalse($verifyVictory->verify());

        $board = new Board();
        $boardState = [
            ['O', '', ''],
            ['', 'O', ''],
            ['', 'X', 'X']
        ];

        $board->updateState($boardState);
        $verifyVictory->setBoard($board);
        $this->assertFalse($verifyVictory->verify());
    }
}