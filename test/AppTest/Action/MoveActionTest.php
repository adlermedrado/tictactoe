<?php

namespace AppTest\Action;

use App\Action\MoveAction;
use App\Domain\Game\Board\Exception\InvalidBoardStructureException;
use Interop\Http\ServerMiddleware\DelegateInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class MoveActionTest extends TestCase
{
    public function testResponseInitialRequest()
    {

        $params = [
            "currentBoardState" => [["", "X", ""], ["", "", ""], ["", "", ""]],
            "player" => '0'
        ];
        $request = $this->prophesize(ServerRequestInterface::class);
        $request->getParsedBody()->willReturn($params);

        $moveAction = new MoveAction();
        $response = $moveAction->process(
            $request->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $json = json_decode((string)$response->getBody());

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Success', $json->message);
        $this->assertEquals([["", "X", ""], ["", "", ""], ["", "", ""]], $json->boardState);
    }
}
