<?php

namespace AppTest\Action;

use App\Action\MoveBotAction;
use Interop\Http\ServerMiddleware\DelegateInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class MoveBotActionTest extends TestCase
{
    public function testResponseInitialRequest()
    {
        $params = [
            "currentBoardState" => [["", "X", ""], ["", "", ""], ["", "", ""]],
            "player" => 'O'
        ];
        $request = $this->prophesize(ServerRequestInterface::class);
        $request->getParsedBody()->willReturn($params);

        $moveBotAction = new MoveBotAction();
        $response = $moveBotAction->process(
            $request->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $json = json_decode((string)$response->getBody());

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Success', $json->message);
    }
}
