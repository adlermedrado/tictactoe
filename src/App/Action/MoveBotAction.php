<?php

namespace App\Action;

use App\Domain\Game\Board\Exception\CellAlreadyUsedException;
use App\Domain\Game\Board\Exception\InvalidBoardStructureException;
use App\Domain\Game\Board\Exception\InvalidCoordinateException;
use App\Domain\Game\Exception\InvalidPlayerException;
use App\Domain\Game\Exception\TheGameIsOverException;
use App\Domain\Game\Player\Bot;
use App\Domain\Game\TicTacToe;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class MoveBotAction implements ServerMiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $params = $request->getParsedBody();

        try {
            $game = new TicTacToe($params);
            $game->play(new Bot());
        } catch (InvalidBoardStructureException |
                 InvalidPlayerException |
                 CellAlreadyUsedException |
                 TheGameIsOverException $e) {
            return new JsonResponse(['message' => $e->getMessage()], 403);
        }

        return new JsonResponse(['message' => 'Success', 'boardState' => $game->getBoard()->toArray()], 200);
    }
}
