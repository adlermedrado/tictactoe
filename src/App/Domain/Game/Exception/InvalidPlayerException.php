<?php

namespace App\Domain\Game\Exception;

class InvalidPlayerException extends \Exception
{
    public function __construct()
    {
        $message = 'The provided player is Invalid';
        parent::__construct($message);
    }
}
