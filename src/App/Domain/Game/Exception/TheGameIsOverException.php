<?php

namespace App\Domain\Game\Exception;

class TheGameIsOverException extends \Exception
{
    public function __construct()
    {
        $message = 'This game is over, what about start a new one?';
        parent::__construct($message);
    }
}