<?php

namespace App\Domain\Game\Player;

interface PlayerInterface
{
    public function setBoardState(array $boardState): array;
    public function __toString();
}