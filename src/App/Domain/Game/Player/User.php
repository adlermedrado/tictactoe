<?php

namespace App\Domain\Game\Player;

use App\Domain\Game\Board\Board;

class User implements PlayerInterface
{
    const PLAYER_TYPE_USER = 'X';

    public function setBoardState(array $boardState): array
    {
        $board = new Board();

        // Just for validation purposes
        $board->updateState($boardState);

        return $board->toArray();
    }

    public function __toString()
    {
        return self::PLAYER_TYPE_USER;
    }
}