<?php

namespace App\Domain\Game;

use App\Domain\Game\Board\Board;
use App\Domain\Game\Board\Exception\InvalidBoardStructureException;
use App\Domain\Game\Player\PlayerInterface;

class TicTacToe
{
    private $lastBoardStateData = [];
    private $board;

    public function __construct(array $params)
    {
        $this->prepareLastBoardState($params);
        $this->setBoard(new Board());
        $this->getBoard()->updateState($this->getLastBoardStateData());
    }

    protected function prepareLastBoardState(array $params): void
    {
        if (Board::validateBoardStructure($params['currentBoardState']) === false) {
            throw new InvalidBoardStructureException();
        }
        $this->lastBoardStateData = $params['currentBoardState'];
    }

    /**
     * @return Board
     */
    public function getBoard(): Board
    {
        return $this->board;
    }

    /**
     * @param Board $board
     */
    public function setBoard(Board $board)
    {
        $this->board = $board;
    }

    public function getLastBoardStateData()
    {
        return $this->lastBoardStateData;
    }

    public function play(PlayerInterface $player)
    {
        $newBoardState = $player->setBoardState($this->getBoard()->toArray());
        $this->getBoard()->updateState($newBoardState);
    }
}
