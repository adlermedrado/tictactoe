<?php

namespace App\Domain\Game;

use App\Domain\Game\Board\Board;
use App\Domain\Game\Exception\TheGameIsOverException;

class Minimax implements MoveInterface
{
    private $boardState;

    public function makeMove(array $boardState, string $playerUnit = 'X'): array
    {
        $this->boardState = $boardState;

        if ($this->checkWinner()) {
            throw new TheGameIsOverException();
        }

        $freePositions = [];

        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 3; $j++) {
                if (empty($boardState[$i][$j])) {
                    $freePositions[] = [$i, $j];
                }
            }
        }

        shuffle($freePositions);
        $freePositions = $freePositions[0];
        $boardState[$freePositions[0]][$freePositions[1]] = $playerUnit;

        return $boardState;
    }

    private function checkWinner()
    {
        $board = new Board();
        $board->updateState($this->boardState);
        $verifyVictory = new VerifyVictory();
        $verifyVictory->setBoard($board);
        return $verifyVictory->verify();
    }
}