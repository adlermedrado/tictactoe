<?php

namespace App\Domain\Game\Board;

use App\Domain\Game\Board\Exception\FullBoardException;
use App\Domain\Game\Player\Bot;
use App\Domain\Game\Player\PlayerInterface;
use App\Domain\Game\Player\User;

class Board
{
    const ROW_LIMIT = 3;
    private $rows = [];

    public function __construct()
    {
        for ($i = 0; $i < self::ROW_LIMIT; $i++) {
            $this->addRow(new Row($i));
        }
    }

    public function addRow(Row $row)
    {
        if (count($this->rows) >= self::ROW_LIMIT) {
            throw new FullBoardException();
        }

        $this->rows[] = $row;
    }

    public static function validateBoardStructure(array $boardInfo): bool
    {
        if (count($boardInfo) != self::ROW_LIMIT) {
            return false;
        }

        foreach ($boardInfo as $board) {
            if (count($board) != Row::CELL_BY_ROW_LIMIT) {
                return false;
            }
        }
        return true;
    }

    public function toArray()
    {
        $board = [];
        foreach ($this->getRows() as $row) {
            $board[] = $row->toArray();
        }

        return $board;
    }

    public function getRows(): array
    {
        return $this->rows;
    }

    public function updateState($stateData): void
    {
        foreach ($stateData as $rowNumber => $rowData) {
            $row = $this->getRow($rowNumber);
            foreach ($rowData as $cellNumber => $cellData) {
                /**
                 * @var $cell Cell
                 */
                $cell = $row->getCell($cellNumber);
                switch ($cellData) {
                    case 'X':
                        $user = new User();
                        $cell->setPlayer($user);
                        break;
                    case 'O':
                        $bot = new Bot();
                        $cell->setPlayer($bot);
                        break;
                    default:
                        // Just to keep the structure
                        break;
                }
            }
        }
    }

    public function getRow($rowNumber)
    {
        return $this->rows[$rowNumber];
    }

    public function setPosition(int $rowNumber, int $cellNumber, PlayerInterface $player)
    {
        $row = $this->getRow($rowNumber);
        $row->getCell($cellNumber)->setPlayer($player);
    }

    public function hasAvailableSpaces()
    {
       foreach ($this->rows as $row) {
           foreach ($row->getCells() as $cell) {
               if (empty($cell->getValue())) {
                   return true;
               }
           }
       }

       return false;
    }
}
