<?php

namespace App\Domain\Game\Board;

use App\Domain\Game\Board\Exception\FullRowException;

class Row
{
    const CELL_BY_ROW_LIMIT = 3;

    /**
     * @var Cell[] Cell Collection
     */
    private $cells = [];

    public function __construct()
    {
        for ($i = 0; $i < self::CELL_BY_ROW_LIMIT; $i++) {
            $this->addCell(new Cell($i));
        }
    }

    public function addCell(Cell $cell)
    {
        if (count($this->cells) >= self::CELL_BY_ROW_LIMIT) {
            throw new FullRowException();
        }

        $this->cells[] = $cell;
    }

    public function getCell($cellNumber)
    {
        return $this->cells[$cellNumber];
    }

    public function getCells(): array
    {
        return $this->cells;
    }

    public function toArray()
    {
        $row = [];
        foreach ($this->getCells() as $cell) {
            $row[] = $cell->getValue();
        }

        return $row;
    }
}
