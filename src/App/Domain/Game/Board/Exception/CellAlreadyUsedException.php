<?php

namespace App\Domain\Game\Board\Exception;

class CellAlreadyUsedException extends \Exception
{
    public function __construct()
    {
        $message = 'The selected Cell is already being used';
        parent::__construct($message);
    }
}
