<?php

namespace App\Domain\Game\Board\Exception;

class IllegalCellPositionException extends \Exception
{
    public function __construct()
    {
        $message = 'Invalid position for Cell' ;
        parent::__construct($message);
    }
}
