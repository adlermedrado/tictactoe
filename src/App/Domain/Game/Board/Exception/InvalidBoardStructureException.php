<?php

namespace App\Domain\Game\Board\Exception;


class InvalidBoardStructureException extends \Exception
{
    public function __construct()
    {
        $message = 'The provided board has an invalid structure';
        parent::__construct($message);
    }
}
