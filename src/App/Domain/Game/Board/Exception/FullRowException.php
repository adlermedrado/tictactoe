<?php

namespace App\Domain\Game\Board\Exception;

class FullRowException extends \Exception
{
    public function __construct()
    {
        $message = 'This row is already complete (max 3 cells)' ;
        parent::__construct($message);
    }
}
