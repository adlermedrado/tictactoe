<?php

namespace App\Domain\Game\Board;

use App\Domain\Game\Board\Exception\CellAlreadyUsedException;
use App\Domain\Game\Board\Exception\IllegalCellPositionException;
use App\Domain\Game\Player\PlayerInterface;
use App\Domain\Game\User;

class Cell
{
    const AVAILABLE_POSITIONS = [0, 1, 2];
    private $position = null;
    private $player;

    public function __construct($position = 0)
    {
        $this->setPosition($position);
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position)
    {
        if (!in_array($position, self::AVAILABLE_POSITIONS)) {
            throw new IllegalCellPositionException();
        }

        if (is_null($this->position) === false) {
            throw new CellAlreadyUsedException();
        }

        $this->position = $position;
    }

    public function getPlayer() : PlayerInterface
    {
        return $this->player;
    }

    public function setPlayer(PlayerInterface $player)
    {
        $this->player = $player;
    }

    public function getValue()
    {
        if ($this->player instanceof PlayerInterface) {
            return $this->player->__toString();
        } else {
            return '';
        }
    }
}
