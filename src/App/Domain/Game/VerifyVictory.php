<?php

namespace App\Domain\Game;

use App\Domain\Game\Board\Board;

class VerifyVictory
{
    /**
     * @var Board Board class instance
     */
    private $board;


    public function verify()
    {
        $hasWinner = [$this->checkColumnsVictory(), $this->checkCellsVictory(), $this->checkDiagonalVictory()];
        return in_array(true, $hasWinner);
    }

    private function checkColumnsVictory()
    {
        $rows = $this->board->getRows();
        for ($i = 0; $i < 3; $i++) {
            if (!empty($rows[0]->getCell($i)->getValue()) &&
                $rows[0]->getCell($i)->getValue() === $rows[1]->getCell($i)->getValue() &&
                $rows[0]->getCell($i)->getValue() === $rows[2]->getCell($i)->getValue()
            ) {
                return true;
            }
        }

        return false;
    }

    private function checkCellsVictory()
    {
        $rows = $this->board->getRows();
        for ($i = 0; $i < 3; $i++) {
            if (!empty($rows[$i]->getCell(0)->getValue()) &&
                $rows[$i]->getCell(0)->getValue() === $rows[$i]->getCell(1)->getValue() &&
                $rows[$i]->getCell(0)->getValue() === $rows[$i]->getCell(2)->getValue()
            ) {
                return true;
            }
        }

        return false;
    }

    private function checkDiagonalVictory()
    {

        $rows = $this->board->getRows();

        if (!empty($rows[0]->getCell(0)->getValue()) &&
            $rows[0]->getCell(0)->getValue() == $rows[1]->getCell(1)->getValue() &&
            $rows[1]->getCell(1)->getValue() == $rows[2]->getCell(2)->getValue()
        ) {
            return true;
        }

        if (!empty($rows[2]->getCell(0)->getValue()) &&
            $rows[2]->getCell(0)->getValue() == $rows[1]->getCell(1)->getValue() &&
            $rows[2]->getCell(0)->getValue() == $rows[0]->getCell(2)->getValue()
        ) {
            return true;
        }

        return false;
    }

    public function setBoard(Board $board)
    {
        $this->board = $board;
    }
}
